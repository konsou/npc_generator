from dataclasses import dataclass
from collections import Counter
from typing import List, Tuple, Dict
from random import choices
import json
# from tabulate import tabulate
from prettytable import PrettyTable
import re
from dataclasses_json import dataclass_json


@dataclass_json
@dataclass
class Item:
    category_major: str
    category_minor: str
    name: str
    value: str
    # value_cp: int
    weight: str

    def __str__(self):
        return f"{self.category_minor}, {self.name} - value {self.value} - weight {self.weight}"


def load_items() -> Tuple[List[Item], Dict[str, List[Item]]]:
    """ Return a tuple of two values:
    First: list of all loaded Items
    Second: dict where keys are the major caregories and items are lists of all
            Items in that category"""
    with open('text_data/items.json') as f:
        items_unprocessed = json.load(f)

    category_lists = dict()

    items_processed = []  # holds Item-dataclass items

    for current_item in items_unprocessed:
        # decode dict to Item dataclass
        item_decoded = Item.from_dict(current_item)
        items_processed.append(item_decoded)

        if item_decoded.category_major not in category_lists:
            category_lists[item_decoded.category_major] = []

        category_lists[item_decoded.category_major].append(item_decoded)

    print(f"Loaded and processed {len(items_processed)} items")

    return items_processed, category_lists


def load_major_categories(items: List[Item]) -> List:
    counter = Counter()

    for item in items:
        counter[item.category_major] += 1

    return list(counter.keys())


if __name__ == '__main__':
    ITEMS, ITEMS_BY_CATEGORY = load_items()
    MAJOR_CATEGORIES = list(ITEMS_BY_CATEGORY.keys())

    print(f"Major categories: {MAJOR_CATEGORIES}")

    items_to_pick = 1
    chosen_category = "ALL"

    while True:
        if chosen_category == "ALL":
            list_to_pick_from = ITEMS
        else:
            list_to_pick_from = ITEMS_BY_CATEGORY[chosen_category]

        chosen_items = choices(list_to_pick_from, k=items_to_pick)

        print()
        # print("---------- RANDOM ITEMS ----------")

        # table = PrettyTable(["Cat MAJOR", "Cat minor", "Name", "Value", "Weight"])
        table = PrettyTable(["Category", "Name", "Value", "Weight"])
        # table = PrettyTable(["Name", "Value"])

        for item in chosen_items:
            if item.category_minor:
                name = f"{item.category_minor} - {item.name}"
            else:
                name = item.name
            table.add_row([item.category_major, name, item.value, item.weight])
            # table.add_row([item.category_major, item.name, item.value, item.weight])
            # table.add_row([item.name, item.value])

        print(table)

        user_input = input("ENTER to generate again, number to generate n items, "
                           "c to select category, s to save to file, q to quit: ").strip().lower()
        if user_input == "q":
            break
        elif user_input == "c":
            while True:
                print(f"Select category:")
                for number, key in enumerate(MAJOR_CATEGORIES):
                    print(f"    {number}: {key}")
                print(f"    {len(MAJOR_CATEGORIES)}: ALL")
                try:
                    user_input_category = int(input(f"    : ").strip())
                except ValueError:
                    continue
                if 0 <= user_input_category <= len(MAJOR_CATEGORIES) - 1:
                    chosen_category = MAJOR_CATEGORIES[user_input_category]
                    break
                elif user_input_category == len(MAJOR_CATEGORIES):
                    chosen_category = "ALL"
                    break
                else:
                    # invalid input
                    pass

            print(f"You chose: {chosen_category}")

        elif user_input == "s":
            output_filename = 'saved_items.txt'
            with open(output_filename, 'w') as f:
                f.write(str(table))
                print(f"Saved items to {output_filename}")

        elif user_input.isnumeric():
            items_to_pick = int(user_input)
        # else:
        #     items_to_pick = 1

