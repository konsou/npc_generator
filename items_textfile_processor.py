import re
import json

from items import Item


def main():
    with open('text_data/items.txt') as f:
        text = f.read()

    lines = text.split('\n')

    major_cat = ""
    minor_cat = ""
    name = ""
    value = ""
    weight = ""

    items_list = []

    def save_item(file, major, minor, name, value="", weight=""):
        save_text = f"MAJOR: {major.strip()}, MINOR: {minor.strip()}, " \
                    f"NAME: {name.strip()}, VALUE: {value.strip()}, WEIGHT: {weight.strip()}\n"
        file.write(save_text)

    def add_item_to_list(items_list, major, minor="", name="", value="", weight=""):
        items_list.append(Item(category_major=major,
                               category_minor=minor,
                               name=name,
                               value=value,
                               weight=weight))

    with open('items_parse_debug_output.txt', 'w') as of:
        with open('items_parse_debug_output_terse.txt', 'w') as oft:
            for line in lines:
                if line.strip():
                    # print('--')

                    # Detect major category
                    try:
                        major_cat = line.split(':')[1].strip()
                        print(major_cat)
                        of.write(f"CATEGORY: {major_cat}\n")
                        continue
                    except IndexError:
                        pass

                    # if line doesn't start with space then it either is a new minor category
                    # or a line without a minor category
                    if not line[0].isspace():
                        minor_cat = ""
                        of.write(f"Line doesn't start with whitespace - minor category cleared\n")

                    # Split the line on two or more spaces
                    split_line = re.split(r'\s{2,}', line)
                    # print(split_line)
                    of.write(f"{split_line}\n")

                    if major_cat == 'FURS':
                        # formatting in furs totally different
                        if split_line[0].lower().startswith('creature'):
                            # skip the heading line
                            continue

                        headings = ('Pelt', 'Trim', 'Cape, Jacket', 'Coat, Robe, Blanket')
                        for index, heading in enumerate(headings, 1):
                            save_item(oft, major_cat, heading,
                                      name=split_line[0],
                                      value=split_line[index])
                            add_item_to_list(items_list, major_cat, heading,
                                             name=split_line[0],
                                             value=split_line[index])
                        continue

                    if len(split_line) == 3:
                        # normal line
                        if split_line[0].strip():
                            # first item actually has some text
                            name, value, weight = split_line
                        else:
                            _, name, value = split_line
                    elif len(split_line) == 4:
                        # normal line after a minor category heading
                        _, name, value, weight = split_line

                    elif len(split_line) == 1:
                        # minor category heading
                        minor_cat = split_line[0]
                        of.write(f"Minor category changed to {minor_cat}\n")
                        continue

                    elif len(split_line) == 2:
                        # minor category heading with "-" as weight
                        # OR normal line with only name and value
                        name, value = split_line

                        if value == "-" or not value.strip():
                            minor_cat = name
                            of.write(f"Minor category changed to {minor_cat}\n")
                            continue

                        name = name.strip()




                    # print(output_text)
                    save_item(of, major_cat, minor_cat, name, value, weight)
                    save_item(oft, major_cat, minor_cat, name, value, weight)
                    add_item_to_list(items_list, major_cat, minor_cat, name, value, weight)

    output_list = []
    for item in items_list:
        output_list.append(item.to_dict())

    with open('text_data/items.json', 'w') as f:
        f.write(json.dumps(output_list, indent=4))


if __name__ == '__main__':
    main()
