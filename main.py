from data import RACES, NAMES, PROFESSIONS, PROFESSION_SUPPORT_VALUES, PERSONAL_ATTRIBUTES
from random import choice, choices


while True:
    race = choice(RACES)
    gender = choice(['male', 'female'])
    try:
        first_name = choice(NAMES[race][gender])
    except KeyError:
        first_name = ""
        # print(f"(no first names available for {race} {gender})")

    try:
        last_name = choice(NAMES[race]['last'])
    except KeyError:
        last_name = ""
        # print(f"(no last names available for {race})")

    profession = choices(PROFESSIONS, weights=PROFESSION_SUPPORT_VALUES)[0]
    attributes = choices(PERSONAL_ATTRIBUTES, k=2)

    print()
    print(f"-------------------")
    print(f"Race:       {race}")
    print(f"Gender:     {gender}")
    print(f"Name:       {first_name} {last_name}")
    print(f"Profession: {profession}")
    print(f"Attributes: {', '.join(attributes)}")
    user_input = input("ENTER to generate again, q to quit: ")
    if user_input.strip().lower() == "q":
        break
