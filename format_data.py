def format_data(input_string):
    split_ = input_string.split(", ")
    for str_ in split_:
        print(f"    '{str_}',")

if __name__ == '__main__':
    format_data("Ambershard, Barrelhelm, Copperhearth, Deepmiddens, Drakantal, Evermead, Garkalan, Grimtor, Hackshield, Irongull, Markolak, Ramcrown, Rockharvest, Silvertarn, Skandalor, Zarkanan")