RACES = [
    'Human',
    'Dragonborn',
    'Dwarf',
    'Elf',
    # 'Half Elf',
    'Halfling',
    'Half Orc',
    'Gnome',
    'Tiefling',
]

GENDERS = ['male', 'female']
NAME_TYPES = ['male', 'female', 'last']

PERSONAL_ATTRIBUTES = [
    'strong',
    'weak',
    'dextrous',
    'clumsy',
    'hardy',
    'fragile',
    'smart',
    'dumb',
    'wise',
    'inexperienced',
    'charismatic',
    'ugly',
]

PROFESSIONS = [
    'Shoemaker',
    'Furrier',
    'Maidservant',
    'Tailor',
    'Barber',
    'Jeweler',
    'Tavern/Restaurant keeper',
    'Old-Clothes',
    'Pastrycook',
    'Mason',
    'Carpenter',
    'Weaver',
    'Chandler',
    'Mercer',
    'Cooper',
    'Baker',
    'Watercarrier',
    'Scabbardmaker',
    'Wine-Seller',
    'Hatmaker',
    'Saddler',
    'Chicken Butcher',
    'Pursemaker',
    'Woodseller',
    'Magic-Shop keeper',
    'Bookbinder',
    'Butcher',
    'Fishmonger',
    'Beer-Seller',
    'Buckle Maker',
    'Plasterer',
    'Spice Merchant',
    'Blacksmith',
    'Painter',
    'Doctor (licensed)',
    'Doctor (unlicensed)',
    'Roofer',
    'Locksmith',
    'Bather',
    'Ropemaker',
    'Innkeeper',
    'Tanner',
    'Copyist',
    'Sculptor',
    'Rugmaker',
    'Harness-Maker',
    'Bleacher',
    'Hay Merchant',
    'Cutler',
    'Glovemaker',
    'Woodcarver',
    'Bookseller',
    'Illuminator',
]

PROFESSION_SUPPORT_VALUES = [
    150,
    250,
    250,
    250,
    350,
    400,
    400,
    400,
    500,
    500,
    550,
    600,
    700,
    700,
    700,
    800,
    850,
    850,
    900,
    950,
    1000,
    1000,
    1100,
    2400,
    2800,
    3000,
    1200,
    1200,
    1400,
    1400,
    1400,
    1400,
    1500,
    1500,
    1700,
    350,
    1800,
    1900,
    1900,
    1900,
    2000,
    2000,
    2000,
    2000,
    2000,
    2000,
    2100,
    2300,
    2300,
    2400,
    2400,
    6300,
    3900,
]


def read_names(race_, name_type_=None):
    """ name_type_ can be: 'male', 'female', 'last' """
    filename = f"text_data\\names_{race_.lower().replace(' ', '_')}_{name_type_}.txt"

    with open(filename) as f:
        names_with_linebreaks = f.readlines()

    names = []

    for name in names_with_linebreaks:
        names.append(name.strip())

    print(f"{len(names)} {race_} {name_type_} names loaded.")

    return names


NAMES = dict()

for race in RACES:
    NAMES[race] = dict()

    for name_type in NAME_TYPES:
        try:
            NAMES[race][name_type] = read_names(race, name_type)
        except OSError:
            pass



